Questions and Answers:
1.  TestIterator.setUp and TestList.setup: Does changing the list to a LinkedList make a difference?
        Changing ArrayList to LinkedList in TestIterator does not stop it from successfully building in the same amount of time.
2.  TestIterator.testRemove: What happens if you use list.remove(Integer.valueOf(77))?
        It would remove from the list, the first Integer of value 77.
3.  TestList.testRemoveObject: What does this method (list.remove(5);) do?
        It removes from the list the variable (in this case Integer) at index 5.
4.  What does (list.remove(Integer.valueOf(5));) do?
        It removes from the list the Integer with the value of 5.